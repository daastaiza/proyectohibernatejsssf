/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Area_DAO_IMP;
import Dao.Area_Dao;
import Moodel.AreaTrabajo;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author LuisF
 */
@ManagedBean
@javax.faces.bean.ViewScoped
public class Area_Bean {

    public Area_Bean() {
        area = new AreaTrabajo();

    }

    private List<AreaTrabajo> listar;

    public List<AreaTrabajo> getListar() {
        Area_Dao aDao = new Area_DAO_IMP();
        listar = aDao.mostrar_areas_trabajo();
        return listar;
    }

    public AreaTrabajo getArea() {
        return area;
    }

    private AreaTrabajo area;

    public void setArea(AreaTrabajo area) {
        this.area = area;
    }
    boolean result;

    public void eliminararea() {
        Area_Dao aDao = new Area_DAO_IMP();
        result = aDao.eliminar_area(area);
        area = new AreaTrabajo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area no eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }
    public void modificararea() {
        Area_Dao aDao = new Area_DAO_IMP();
        result = aDao.modificar_area(area);
        area = new AreaTrabajo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
    
    
    
    
    public void nuevaarea() {
        Area_Dao aDao = new Area_DAO_IMP();
        result = aDao.nueva_area(area);
        area = new AreaTrabajo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area registrada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }

    public void setListar(List<AreaTrabajo> listar) {
        this.listar = listar;
    }
    
    public List<SelectItem> listarareas;
    
    public List<SelectItem> getlistarareas(){
        this.listarareas = new ArrayList<SelectItem>();
        Area_Dao eDao = new Area_DAO_IMP();
        List<AreaTrabajo> a =  eDao.mostrar_areas_trabajo();
        listarareas.clear();
        for(AreaTrabajo area : a){
            SelectItem areasitem =new SelectItem(area.getIdAreaT(),area.getNombre());
            this.listarareas.add(areasitem);
        }
        return listarareas;
        
    }

}
