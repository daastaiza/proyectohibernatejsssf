/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Cargo_Dao;
import Dao.Cargo_Dao_IMP;
import Moodel.Cargo;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;

import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author REDP
 */
@ManagedBean
@javax.faces.bean.ViewScoped
public class Cargo_Bean {

    public Cargo getCargo() {
        return cargo;
    }

    /**
     * Creates a new instance of Cargo_Bean
     * @param cargo
     */
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

     private Cargo  cargo;
    public Cargo_Bean() {
         cargo = new Cargo();
    }
     private List<Cargo> listar;
     public List<Cargo> getListar() {
        Cargo_Dao aDao = new Cargo_Dao_IMP();
        listar = aDao.listar_cargo();
        return listar;
    }
    boolean result;
    public void eliminararea() {
        Cargo_Dao aDao = new Cargo_Dao_IMP();
        result = aDao.eliminar_cargo(cargo);
        cargo = new Cargo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area no eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }
    public void modificar_cargo() {
        Cargo_Dao aDao = new Cargo_Dao_IMP();
        result = aDao.modificar_cargo(cargo);
        cargo = new Cargo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
    public void nuevo_cargo() {
        Cargo_Dao aDao = new Cargo_Dao_IMP();
        result = aDao.nueva_cargo(cargo);
        cargo = new Cargo();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area registrada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
    public void setListar(List<Cargo> listar) {
        this.listar = listar;
    }
     public List<SelectItem> listarcargo;
    
    public List<SelectItem> getlistarcargos(){
        this.listarcargo = new ArrayList<SelectItem>();
        Cargo_Dao eDao = new Cargo_Dao_IMP();
        List<Cargo> a =  eDao.listar_cargo();
        listarcargo.clear();
        for(Cargo cargo : a){
            SelectItem cargositem =new SelectItem(cargo.getIdCargo(),cargo.getNombre());
            this.listarcargo.add(cargositem);
        }
        return listarcargo;
        
    }
     
     
     
     
     
     
    
}
