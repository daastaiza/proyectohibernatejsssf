/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Ciudad_Dao;
import Dao.Ciudad_Dao_IMP;
import Moodel.Ciudad;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author REDP
 */
@ManagedBean
@javax.faces.bean.ViewScoped

public class Ciudad_Bean {

    public Ciudad_Bean() {
        ciudad = new Ciudad();

    }
    private List<Ciudad> listar;

    public List<Ciudad> getListar() {
        Ciudad_Dao aDao = new Ciudad_Dao_IMP();
        listar = aDao.listar_ciudad();
        return listar;
    }
      public Ciudad getCiudad() {
        return ciudad;
    }

    private Ciudad ciudad;

    public void setCiudad(Ciudad area) {
        this.ciudad = ciudad;
    }
    boolean result;
    public void eliminar_ciudad() {
        Ciudad_Dao aDao = new Ciudad_Dao_IMP();
        result = aDao.eliminar_ciudad(ciudad);
        ciudad = new Ciudad();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area no eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }
    public void modificar_ciudad() {
        Ciudad_Dao aDao = new Ciudad_Dao_IMP();
        result = aDao.modificar_ciudad(ciudad);
        ciudad = new Ciudad();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
     public void nueva_ciudad() {
         Ciudad_Dao aDao = new Ciudad_Dao_IMP();
        result = aDao.nuevo_ciudad(ciudad);
        ciudad = new Ciudad();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area registrada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
      public void setListar(List<Ciudad> listar) {
        this.listar = listar;
    }
     public List<SelectItem> listarciudad;
    
    public List<SelectItem> getlistarciudades(){
        this.listarciudad = new ArrayList<SelectItem>();
        Ciudad_Dao eDao = new Ciudad_Dao_IMP();
        List<Ciudad> a =  eDao.listar_ciudad();
        listarciudad.clear();
        for(Ciudad ciudad : a){
            SelectItem areasitem =new SelectItem(ciudad.getIdCiudad(),ciudad.getNombre());
            this.listarciudad.add(areasitem);
        }
        return listarciudad;
        
    }
}
