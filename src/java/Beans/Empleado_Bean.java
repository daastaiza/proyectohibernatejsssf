/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Empleado_Dao;
import Dao.Empleado_Dao_IMP;

import Moodel.AreaTrabajo;
import Moodel.Cargo;

import Moodel.Empleado;
import Moodel.Estado;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author REDP
 */
@ManagedBean
@javax.faces.bean.ViewScoped
public class Empleado_Bean {

    /**
     * Creates a new instance of Empleado_Bean
     */
    public Empleado_Bean() {
        empleado = new Empleado();

    }

    private List<Empleado> listar;

    public List<Empleado> getListar() {
        Empleado_Dao aDao = new Empleado_Dao_IMP();
        listar = aDao.listar_empleado();
        return listar;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    private Empleado empleado;

    public AreaTrabajo getArea() {
        return area;
    }

    public void setArea(AreaTrabajo area) {
        this.area = area;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }


    private AreaTrabajo area;
    private Cargo cargo;
    private Estado estado;

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    boolean result;

    public void eliminarempleado() {
        Empleado_Dao aDao = new Empleado_Dao_IMP();
        result = aDao.eliminar_empleado(empleado);
        empleado = new Empleado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Empleado eliminada!',\n"
                    + "    content: 'empleado eliminado correctamente',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'empleado no eliminado!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }

    public void modificarempleado() {
       Empleado_Dao aDao = new Empleado_Dao_IMP();
        result = aDao.modificar_empleado(empleado);
        empleado = new Empleado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModalupdate').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }

    public void nuevoempleado() {
        Empleado_Dao aDao = new Empleado_Dao_IMP();
        result = aDao.nuevo_empleado(empleado);
        empleado = new Empleado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Empleado registrado!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Empleado no registrado!',\n"
                    + "    content: 'registro no insertado correctamente en la base de datos',\n"
                    + "    type: 'red',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        }

    }

    public void setListar(List<Empleado> listar) {
        this.listar = listar;
    }

    
public List<SelectItem> listarempleados;
    
    public List<SelectItem> getlistarempleados(){
        this.listarempleados = new ArrayList<SelectItem>();
        Empleado_Dao eDao = new Empleado_Dao_IMP();
        List<Empleado> a =  eDao.listar_empleado();
        listarempleados.clear();
        for(Empleado empleado : a){
            SelectItem empleadoitem =new SelectItem(empleado.getIdEmpleado(),empleado.getNombres());
            this.listarempleados.add(empleadoitem);
        }
        return listarempleados;
        
    }
}
