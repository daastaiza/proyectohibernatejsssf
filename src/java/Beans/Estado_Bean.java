/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Area_DAO_IMP;
import Dao.Estado_Dao;
import Dao.Estado_Dao_IMP;
import Moodel.Cargo;
import Moodel.Estado;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;
import org.primefaces.context.RequestContext;

/**
 *
 * @author REDP
 */
@ManagedBean
@javax.faces.bean.ViewScoped
public class Estado_Bean {

    /**
     * Creates a new instance of Estado_Bean
     */
    private Estado estado;

    public Estado_Bean() {
        estado = new Estado();
    }
    private List<Estado> listar;

    public List<Estado> getListar() {
        Estado_Dao aDao = new Estado_Dao_IMP();
        listar = aDao.listar_estado();
        return listar;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setArea(Estado estado) {
        this.estado = estado;
    }
    boolean result;

    public void eliminar_estado() {
        Estado_Dao aDao = new Estado_Dao_IMP();
        result = aDao.eliminar_estado(estado);
        estado = new Estado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area no eliminada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }

    public void modificarestado() {
        Estado_Dao aDao = new Estado_Dao_IMP();
        result = aDao.modificar_estado(estado);
        estado = new Estado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }

    public void nuevoestado() {
        Estado_Dao aDao = new Estado_Dao_IMP();
        result = aDao.nuevo_estado(estado);
        estado = new Estado();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area registrada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }

    public void setListar(List<Estado> listar) {
        this.listar = listar;
    }
    public List<SelectItem> listarestado;
    
    public List<SelectItem> getlistarestados(){
        this.listarestado = new ArrayList<SelectItem>();
        Estado_Dao eDao = new Estado_Dao_IMP();
        List<Estado> a =  eDao.listar_estado();
        listarestado.clear();
        for(Estado estado : a){
            SelectItem estadoitem =new SelectItem(estado.getIdEstado(),estado.getNombre());
            this.listarestado.add(estadoitem);
        }
        return listarestado;
        
    }

}
