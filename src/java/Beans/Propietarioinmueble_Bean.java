/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Dao.Propetiario_Inmueble_Dao;
import Dao.Propetiario_Inmueble_Dao_IMP;
import Moodel.PropietarioInmueble;
import java.util.List;
import javax.faces.bean.ManagedBean;
import org.primefaces.context.RequestContext;

/**
 *
 * @author LuisF
 */
@ManagedBean
@javax.faces.bean.ViewScoped
public class Propietarioinmueble_Bean {

    private PropietarioInmueble propitario;

    public Propietarioinmueble_Bean() {
        propitario = new PropietarioInmueble();

    }
    private List<PropietarioInmueble> listar;

    public List<PropietarioInmueble> getListar() {
        Propetiario_Inmueble_Dao aDao = new Propetiario_Inmueble_Dao_IMP();
        listar = aDao.listar_propietarioinmueble();
        return listar;
    }

    public PropietarioInmueble getPropietarioInmueble() {
        return propitario;
    }

    public void setPropietarioInmueble(PropietarioInmueble propitario) {
        this.propitario = propitario;
    }
    boolean result;

    public void eliminar_propietarioinmueble() {
        Propetiario_Inmueble_Dao aDao = new Propetiario_Inmueble_Dao_IMP();
        result = aDao.eliminar_propietarioinmueble(propitario);
        propitario = new PropietarioInmueble();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Inquilino SI eliminada!',\n"
                    + "    content: 'registro eliminado de la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModaldelete').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Inquilino no eliminado!',\n"
                    + "    content: 'inquilino eliminado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formdelete\").reset();");
        }

    }

    public void modificar_propietarioinmueble() {
        Propetiario_Inmueble_Dao aDao = new Propetiario_Inmueble_Dao_IMP();
        result = aDao.modificar_propietarioinmueble(propitario);
        propitario = new PropietarioInmueble();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModalupdate').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area modificada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"formupdate\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }

    public void nuevo_propietarioinmueble() {
        Propetiario_Inmueble_Dao aDao = new Propetiario_Inmueble_Dao_IMP();
        result = aDao.nuevo_propietarioinmueble(propitario);
        propitario = new PropietarioInmueble();
        if (result) {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute(""
                    + "$('#myModal').modal('hide');"
                    + "		$.confirm({\n"
                    + "                    				 icon: 'fa fa-check-circle ',\n"
                    + "    title: 'Area registrada!',\n"
                    + "    content: 'registro insertado correctamente en la base de datos',\n"
                    + "    type: 'green',\n"
                    + "    typeAnimated: true,\n"
                    + "\n"
                    + "    buttons: {\n"
                    + "        tryAgain: {\n"
                    + "            text: 'confirmar',\n"
                    + "            btnClass: 'btn-green',\n"
                    + "            action: function(){\n"
                    + "            }\n"
                    + "        }\n"
                    + "        \n"
                    + "    }\n"
                    + "});"
                    + "document.getElementById(\"form\").reset();");
        } else {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("alert(\"no registro!!\");");
        }

    }
}
