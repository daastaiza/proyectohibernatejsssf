/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.AreaTrabajo;
import Util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author LuisF
 */
public class Area_DAO_IMP implements Area_Dao {

    @Override
    public List<AreaTrabajo> mostrar_areas_trabajo() {
       List<AreaTrabajo> listarareas = null;
       Session session = HibernateUtil.getSessionFactory().openSession();
       Transaction transaction = session.beginTransaction();
       String hql = "FROM AreaTrabajo";
        try {
            listarareas = session.createQuery(hql).list();
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            transaction.rollback();
        }
        return listarareas;
       
    }

    @Override
    public boolean nueva_area(AreaTrabajo a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean modificar_area(AreaTrabajo a) {
       boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean eliminar_area(AreaTrabajo a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }
    
}
