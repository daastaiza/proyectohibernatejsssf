/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.AreaTrabajo;
import java.util.List;

/**
 *
 * @author LuisF
 */
public interface Area_Dao {
    //metodo para mostrar lista de empleados
    
    public List<AreaTrabajo> mostrar_areas_trabajo();
    
    public boolean nueva_area(AreaTrabajo a);
    public boolean modificar_area(AreaTrabajo a);
    public boolean eliminar_area(AreaTrabajo a);
    
}
