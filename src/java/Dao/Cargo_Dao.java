/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Cargo;
import java.util.List;

/**
 *
 * @author REDP
 */
public interface Cargo_Dao {
    public List<Cargo> listar_cargo();
    
    public boolean nueva_cargo(Cargo a);
    public boolean modificar_cargo(Cargo a);
    public boolean eliminar_cargo(Cargo a);
}
