/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Ciudad;
import java.util.List;

/**
 *
 * @author REDP
 */
public interface Ciudad_Dao {
    
    public List<Ciudad> listar_ciudad();

    public boolean nuevo_ciudad(Ciudad a);

    public boolean modificar_ciudad(Ciudad a);

    public boolean eliminar_ciudad(Ciudad a);
}
