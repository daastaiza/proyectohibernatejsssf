/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Ciudad;
import Util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author REDP
 */
public class Ciudad_Dao_IMP implements Ciudad_Dao {

    @Override
    public List<Ciudad> listar_ciudad() {
        List<Ciudad> listarciudades = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        String hql = "FROM Ciudad";
        try {
            listarciudades = session.createQuery(hql).list();
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            transaction.rollback();
        }
        return listarciudades;
    }

    @Override
    public boolean nuevo_ciudad(Ciudad a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean modificar_ciudad(Ciudad a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean eliminar_ciudad(Ciudad a) {
    boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;    
    }
}
