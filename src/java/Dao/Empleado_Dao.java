/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Empleado;
import java.util.List;

/**
 *
 * @author REDP
 */
public interface Empleado_Dao {

    public List<Empleado> listar_empleado();

    public boolean nuevo_empleado(Empleado a);

    public boolean modificar_empleado(Empleado a);

    public boolean eliminar_empleado(Empleado a);
}
