/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Estado;
import java.util.List;

/**
 *
 * @author REDP
 */
public interface Estado_Dao {
     
    public List<Estado> listar_estado();
    
    public boolean nuevo_estado(Estado a);
    public boolean modificar_estado(Estado a);
    public boolean eliminar_estado(Estado a);
}
