/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Estado;
import Util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author REDP
 */
public class Estado_Dao_IMP implements Estado_Dao{

    @Override
    public List<Estado> listar_estado() {
        List<Estado> listarestado = null;
       Session session = HibernateUtil.getSessionFactory().openSession();
       Transaction transaction = session.beginTransaction();
       String hql = "FROM Estado";
        try {
            listarestado = session.createQuery(hql).list();
            transaction.commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            transaction.rollback();
        }
        return listarestado;
    }

    @Override
    public boolean nuevo_estado(Estado a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean modificar_estado(Estado a) {
        boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }

    @Override
    public boolean eliminar_estado(Estado a) {
         boolean result;
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(a);
            session.getTransaction().commit();
            result = true;
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            session.getTransaction().rollback();
            result = false;
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return result;
    }
    
}
