/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.Inquilino;
import java.util.List;

/**
 *
 * @author REDP
 */
public interface Inquilino_Dao {
    public List<Inquilino> listar_inquilino();
    
    public boolean nuevo_inquilino(Inquilino a);
    public boolean modificar_inquilino(Inquilino a);
    public boolean eliminar_inquilino(Inquilino a);
}
