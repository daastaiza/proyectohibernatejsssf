/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Moodel.PropietarioInmueble;
import java.util.List;

/**
 *
 * @author LuisF
 */
public interface Propetiario_Inmueble_Dao {

    public List<PropietarioInmueble> listar_propietarioinmueble();

    public boolean nuevo_propietarioinmueble(PropietarioInmueble a);

    public boolean modificar_propietarioinmueble(PropietarioInmueble a);

    public boolean eliminar_propietarioinmueble(PropietarioInmueble a);
}
