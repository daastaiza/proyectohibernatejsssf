package Moodel;
// Generated 03-oct-2017 17:41:59 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * DetalleInmueble generated by hbm2java
 */
public class DetalleInmueble  implements java.io.Serializable {


     private Long idDetalleI;
     private Inmueble inmueble;
     private InvInmueble invInmueble;
     private Date fecha;

    public DetalleInmueble() {
    }

    public DetalleInmueble(Inmueble inmueble, InvInmueble invInmueble, Date fecha) {
       this.inmueble = inmueble;
       this.invInmueble = invInmueble;
       this.fecha = fecha;
    }
   
    public Long getIdDetalleI() {
        return this.idDetalleI;
    }
    
    public void setIdDetalleI(Long idDetalleI) {
        this.idDetalleI = idDetalleI;
    }
    public Inmueble getInmueble() {
        return this.inmueble;
    }
    
    public void setInmueble(Inmueble inmueble) {
        this.inmueble = inmueble;
    }
    public InvInmueble getInvInmueble() {
        return this.invInmueble;
    }
    
    public void setInvInmueble(InvInmueble invInmueble) {
        this.invInmueble = invInmueble;
    }
    public Date getFecha() {
        return this.fecha;
    }
    
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }




}


