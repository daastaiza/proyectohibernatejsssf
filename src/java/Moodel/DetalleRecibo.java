package Moodel;
// Generated 03-oct-2017 17:41:59 by Hibernate Tools 4.3.1



/**
 * DetalleRecibo generated by hbm2java
 */
public class DetalleRecibo  implements java.io.Serializable {


     private Long idDetalleR;
     private Inquilino inquilino;
     private ReciboPago reciboPago;
     private long valorTotalSaldo;

    public DetalleRecibo() {
    }

    public DetalleRecibo(Inquilino inquilino, ReciboPago reciboPago, long valorTotalSaldo) {
       this.inquilino = inquilino;
       this.reciboPago = reciboPago;
       this.valorTotalSaldo = valorTotalSaldo;
    }
   
    public Long getIdDetalleR() {
        return this.idDetalleR;
    }
    
    public void setIdDetalleR(Long idDetalleR) {
        this.idDetalleR = idDetalleR;
    }
    public Inquilino getInquilino() {
        return this.inquilino;
    }
    
    public void setInquilino(Inquilino inquilino) {
        this.inquilino = inquilino;
    }
    public ReciboPago getReciboPago() {
        return this.reciboPago;
    }
    
    public void setReciboPago(ReciboPago reciboPago) {
        this.reciboPago = reciboPago;
    }
    public long getValorTotalSaldo() {
        return this.valorTotalSaldo;
    }
    
    public void setValorTotalSaldo(long valorTotalSaldo) {
        this.valorTotalSaldo = valorTotalSaldo;
    }




}


