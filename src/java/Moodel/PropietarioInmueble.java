package Moodel;
// Generated 03-oct-2017 17:41:59 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * PropietarioInmueble generated by hbm2java
 */
public class PropietarioInmueble  implements java.io.Serializable {


     private long idPropietario;
     private Empleado empleado;
     private Estado estado;
     private String nombres;
     private String apellidos;
     private long edad;
     private String direccion;
     private String correo;
     private long telefono;
     private long celular;
     private Set<Contrato> contratos = new HashSet<Contrato>(0);
     private Set<Inmueble> inmuebles = new HashSet<Inmueble>(0);

    public PropietarioInmueble() {
    }

	
    public PropietarioInmueble(long idPropietario, Empleado empleado, Estado estado, String nombres, String apellidos, long edad, String direccion, String correo, long telefono, long celular) {
        this.idPropietario = idPropietario;
        this.empleado = empleado;
        this.estado = estado;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.edad = edad;
        this.direccion = direccion;
        this.correo = correo;
        this.telefono = telefono;
        this.celular = celular;
    }
    public PropietarioInmueble(long idPropietario, Empleado empleado, Estado estado, String nombres, String apellidos, long edad, String direccion, String correo, long telefono, long celular, Set<Contrato> contratos, Set<Inmueble> inmuebles) {
       this.idPropietario = idPropietario;
       this.empleado = empleado;
       this.estado = estado;
       this.nombres = nombres;
       this.apellidos = apellidos;
       this.edad = edad;
       this.direccion = direccion;
       this.correo = correo;
       this.telefono = telefono;
       this.celular = celular;
       this.contratos = contratos;
       this.inmuebles = inmuebles;
    }
   
    public long getIdPropietario() {
        return this.idPropietario;
    }
    
    public void setIdPropietario(long idPropietario) {
        this.idPropietario = idPropietario;
    }
    public Empleado getEmpleado() {
        return this.empleado;
    }
    
    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    public Estado getEstado() {
        return this.estado;
    }
    
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    public String getNombres() {
        return this.nombres;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public String getApellidos() {
        return this.apellidos;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    public long getEdad() {
        return this.edad;
    }
    
    public void setEdad(long edad) {
        this.edad = edad;
    }
    public String getDireccion() {
        return this.direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getCorreo() {
        return this.correo;
    }
    
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public long getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }
    public long getCelular() {
        return this.celular;
    }
    
    public void setCelular(long celular) {
        this.celular = celular;
    }
    public Set<Contrato> getContratos() {
        return this.contratos;
    }
    
    public void setContratos(Set<Contrato> contratos) {
        this.contratos = contratos;
    }
    public Set<Inmueble> getInmuebles() {
        return this.inmuebles;
    }
    
    public void setInmuebles(Set<Inmueble> inmuebles) {
        this.inmuebles = inmuebles;
    }




}


